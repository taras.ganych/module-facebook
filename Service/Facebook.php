<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_Facebook
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */

namespace SapientPro\Facebook\Service;

use Exception;
use Facebook\Exceptions\FacebookSDKException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\ScopeInterface;
use SapientPro\Facebook\Api\FacebookInterface;
use Facebook\Facebook as FacebookSdk;

/**
 * Class Notify
 * @package SapientPro\Core\Cron
 */
class Facebook implements FacebookInterface
{
    /**
     * @var NotifierInterface
     */
    private $sdk;

    /**
     * Facebook constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $enCrypt
     * @throws FacebookSDKException
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $enCrypt
    ) {
        $this->sdk = new FacebookSdk([
            'app_id' => $scopeConfig->getValue(
                'sapientpro/facebook/app_id',
                ScopeInterface::SCOPE_STORE
            ),
            'app_secret' => $enCrypt->decrypt(
                $scopeConfig->getValue(
                    'sapientpro/facebook/app_secret',
                    ScopeInterface::SCOPE_STORE
                )
            )
        ]);
    }

    /**
     * @return \Facebook\Facebook
     * @throws Exception
     */
    public function getSdk() : \Facebook\Facebook
    {
        if ($this->sdk instanceof \Facebook\Facebook) {
            return $this->sdk;
        }

        throw new Exception();
    }
}
