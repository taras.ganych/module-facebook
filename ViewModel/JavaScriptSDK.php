<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_Facebook
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */

namespace SapientPro\Facebook\ViewModel;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\Locale\ResolverInterface as LocaleResolverInterface;

/**
 * Class JavaScriptSDK
 * @package SapientPro\Facebook\ViewModel
 */
class JavaScriptSDK extends DataObject implements ArgumentInterface
{
    /**
     * Current API version
     */
    const API_VERSION = 'v6.0';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var LocaleResolverInterface
     */
    private $localeResolver;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param LocaleResolverInterface $localeResolver
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LocaleResolverInterface $localeResolver
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->localeResolver = $localeResolver;
    }

    /**
     * @return string
     */
    public function getVersion() : string
    {
        return self::API_VERSION;
    }

    /**
     * @return mixed
     */
    public function getAppId() : string
    {
        return $this->scopeConfig->getValue('sapientpro/facebook/app_id');
    }

    /**
     * @return mixed
     */
    public function getLocaleCode()
    {
        return $this->localeResolver->getLocale();
    }

}
