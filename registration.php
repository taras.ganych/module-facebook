<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_Instagram
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'SapientPro_Facebook',
    __DIR__
);