<?php
/**
 * SapientPro
 *
 * @category    SapientPro
 * @package     SapientPro_Facebook
 * @author      SapientPro Team <info@sapient.pro >
 * @copyright   Copyright © 2009-2019 SapientPro (https://sapient.pro)
 */

namespace SapientPro\Facebook\Api;

use Magento\Framework\Notification\NotifierInterface;
use Magento\Framework\Message\ManagerInterface;
use Facebook\Facebook as FacebookSdk;

/**
 * Interface FacebookInterface
 * @package SapientPro\Facebook\Api
 */
interface FacebookInterface
{
    /**
     * @return mixed
     */
    public function getSdk() : FacebookSdk;
}
